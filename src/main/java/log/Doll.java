package log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Doll  implements AutoCloseable {

    private String name;
    private LocalDateTime dateCreate;
    private LocalDateTime dateClose;
    private PrintWriter file;
    private  int numberObj;
    private static int counterStudent =0;


    public Doll(String name) {
        this.name=name;
        counterDoll++;
        numberObj= counterDoll;
        this.dateCreate= LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        System.out.println();
        System.out.println("Object name:"+ this.name);
        System.out.println("Object number:"+ this.numberObj);
        System.out.println("Creation time:"+ this.dateCreate.format(formatter));
        try{
            file=new PrintWriter (fileName:this.name+ ".txt");
            this.dateCreate= LocalDateTime.now();
            file.println();
            file.println("Object name:"+ this.name);
            file.println("Object number:"+ this.numberObj);
            file.println("Creation time:"+ this.dateCreate.format(formatter));}
        catch (FileNotFoundException e){
            System.out.println("File not create");
        }
    }

    public Doll(){
        counterDoll++;
        this.name="NoName"+counterDoll;
        numberObj=counterDoll;
        this.dateCreate= LocalDateTime.now();
        DateTimeFormatter formatter= DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        System.out.println();
        System.out.println("Object name:"+ this.name);
        System.out.println("Object number:"+ this.numberObj);
        System.out.println("Creation time:"+ this.dateCreate.format(formatter));
        try{
            file=new PrintWriter(fileName:this.name+ ".txt");
            file.println();
            file.println("Object name:"+ this.name);
            file.println("Object number:"+ this.numberObj);
            file.println("Creation time:"+ this.dateCreate.format(formatter));}
        catch (FileNotFoundException e){
            System.out.println("File not create");
        }
    }
    public void close() throws IOException{
        if(counterDoll== 6) {
            throw new IOException("CLOSE");
        }
        this.dateClose= LocalDateTime.now)();
        DateTimeFormatter formatter =DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        file.println("===");
        file.println(" Close object "+ this.name);
        file.println("Closing time:"+ this.dateClose.format(formatter));

        if(file != null){
            file.println();
            file.println("Closing time:"+ this.dateClose.format(formatter));
            file.close();
        }
    }
}